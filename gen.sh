#!/usr/bin/env zsh

NUM=5
GENERATOR=./bin/GraphGenerator.py
DATA_DIR=./data

mkdir -p ${DATA_DIR}

types=("cu" "cr" "r0.5u" "r0.5r" "mu" "mr")

for tp in "${types[@]}"
do
	for v in {2..12}
	do
		n=$(($v**2))
		for t in {2..14..2}
		do
			for i in {1..${NUM}}
			do
			$GENERATOR -v $n -t $t --type $tp > ${DATA_DIR}/${tp}_${n}_${t}_${i}.net
			done
		done
	done
done

echo "DONE!!"
