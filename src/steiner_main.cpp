#include "steiner.h"
#include <cstdlib>
#include <iostream>
#include <ctime>
#include "scip_exception.hpp"

using namespace std;
using namespace steiner;

/** main function for queens example */
int main(int argc, char ** argv){
	cout << "********************************************" << endl;
	cout << "* Steiner Tree solver based on SCIP        *" << endl;
	cout << "*                                          *" << endl;
	cout << "* (c) Hitoshi Otsuki (2015)                *" << endl;
	cout << "********************************************" << endl << endl;

	if (argc < 2){
		cerr<<"Usage: ./steiner <filename>"<<endl;
		exit(1);
	}

	try {
		clock_t begin = clock();
		// initialize the steiner solver
		SteinerTreeSolver solver(argv[1]);

		// solve the steiner problem
		solver.solve();
		clock_t end = clock();
		double elapsed_secs = double(end-begin)/CLOCKS_PER_SEC;
		// display the solution on stdout
		solver.disp();
		// cout<<"disp@ end"<<endl;
		cout<<"Elapsed time: "<<elapsed_secs<<"sec"<<endl;

	} catch(SCIPException& exc){
		cerr << exc.what() << endl;
		exit(exc.getRetcode());
	}
	return EXIT_SUCCESS;
}
