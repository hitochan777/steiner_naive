#include <iostream>
#include <vector>
#include <set>
#include <algorithm>

using namespace std;

int cnt = 0;
void printVector(const vector<int>& vec){
	int n = vec.size();
	for(int i = 0; i<n;++i){
		cout<<vec[i]<<" ";	
	}
	cout<<endl;
}

void enumerate(vector<int>& cur, const vector<int>& v, const vector<int>& t, int pos){
	int sz = cur.size();
	for(int i = pos; i < (int)t.size(); ++i){
		if(sz < t.size()/2){
			cur.push_back(t[i]);
			for(long long mask = 0;mask<1LL<<(v.size());++mask){
				long long m = mask;		
				// vector<int> diff(t.size()-(sz+1));
				vector<int> curtemp(cur);
				// vector<int>::iterator it;
				// it=set_difference (t.begin(), t.end(), cur.begin(), cur.end(), diff.begin());
				for(int j = 0;j<v.size();++j){
					if(m&1LL){
						curtemp.push_back(v[j]);	
					}	
					// else{
					// 	diff.push_back(v[j]);
					// }
					m>>=1;
				}
				printVector(curtemp);
				// printVector(diff);
				cout<<endl;
			}
			enumerate(cur,v,t, i+1);//recursion
			cur.pop_back();
		}
	}
}

int main(){
	vector<int> v{1,2,3,4,5,6};
	vector<int> t{1,3,4};
	vector<int> diff(v.size()-t.size());
	vector<int> vec{};
	set_difference (v.begin(), v.end(), t.begin(), t.end(), diff.begin());
	enumerate(vec, diff, t, 0);
}
