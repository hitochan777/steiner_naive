#include "FlowEdge.h"

FlowEdge::FlowEdge(int v, int w, int capacity)
:_v(v),_w(w),_capacity(capacity)
{}

FlowEdge::~FlowEdge(){}

int FlowEdge::from(){
	return _v;
}

int FlowEdge::to(){
	return _w;
}

int FlowEdge::capacity(){
	return _capacity;
}

int FlowEdge::other(int vertex){
	if(vertex==_v){
		return _w;
	}
	else if(vertex==_w){
		return _v;
	}
	else{
		throw std::invalid_argument("Invalid vertex number");
	}
	return 0;//this line will never be excecuted.
}
