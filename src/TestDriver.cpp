#include <iostream>
#include <stdexcept>
#include "SteinerTrees.h"
using namespace std;

int main(int argc, char**argv){
	if(argc < 2){
		cerr<<"Usage: TestDriver <filename>"<<endl;
		return -1;
	}
	SteinerTrees sg = SteinerTrees(argv[1]);
	cout<<sg.toString()<<endl;;
	return 0;
}
