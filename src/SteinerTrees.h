#ifndef STEINER_TREES_H
#define STEINER_TREES_H

#include "Graph.h"
#include <set>

class SteinerTrees{
	private:
		int _T;
		std::vector<std::set<int> > _terminals;
		std::vector<int> _bw;//bandwidth
		void addTerminal(int tree, int terminal);

	public:
		Graph graph;
		SteinerTrees(int V, int T);
		SteinerTrees(char* filename);
		void validateTreeNumber(int tree);
		~SteinerTrees();
		std::string toString();
		int T();
		int getBandWidth(int tree);
		std::set<int>& getTerminals(int tree);
};
#endif


