#ifndef STEINER_TREE_H
#define STEINER_TREE_H

#include <vector>
#include <iostream>
#include <map>
#include <iterator>
#include <sstream>
#include <algorithm>
#include "SteinerTrees.h"

#include <scip/scip.h>
#include <scip/scipdefplugins.h>

namespace steiner{
	class SteinerTreeSolver{
		private:
			SCIP* _scip;
			SteinerTrees* st;
			std::vector<std::map<int, std::map<int, std::vector<SCIP_VAR *> > > > _x;
			std::vector<SCIP_CONS *> _cons;
		public:
			SteinerTreeSolver(char* filename);
			~SteinerTreeSolver();
			void solve(void);
			void disp(std::ostream & out = std::cout);
			void enumerate(int tree, std::vector<bool>& side, std::vector<int>& cur, std::vector<int>& v, std::vector<int>& t, int pos);
			void printVector(const std::vector<int>& vec);

	};
}
#endif
