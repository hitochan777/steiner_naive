#include "steiner.h"
#include "scip_exception.hpp"

using namespace std;
using namespace steiner;

/* constructor */
steiner::SteinerTreeSolver::SteinerTreeSolver(char *filename){
	st = new SteinerTrees(filename);

	// initialize scip
	SCIP_CALL_EXC( SCIPcreate(& _scip) );

	// load default plugins linke separators, heuristics, etc.
	SCIP_CALL_EXC( SCIPincludeDefaultPlugins(_scip) );

	// disable scip output to stdout
	SCIP_CALL_EXC( SCIPsetMessagehdlr(_scip, NULL) );

	// create an empty problem
	SCIP_CALL_EXC( SCIPcreateProb(_scip, "steiner", NULL, NULL, NULL, NULL, NULL, NULL, NULL) );

	// set the objective sense to maximize, default is minimize
	SCIP_CALL_EXC( SCIPsetObjsense(_scip, SCIP_OBJSENSE_MINIMIZE) );

	_x.resize(st->T());

	// create a binary variable x(t)_{ij}
	ostringstream namebuf;

	for(int t = 0; t < st->T();++t){
		for (int i = 1; i <= st->graph.V(); ++i){
			for(map<int, vector<FlowEdge> >::iterator it = st->graph.adj(i).begin(); it != st->graph.adj(i).end(); ++it){
				if(i >= it->first){
					continue;
				}
				for(unsigned int k = 0; k < it->second.size(); k++ ){
					SCIP_VAR * var;
					namebuf.str("");
					namebuf << "x(" << t << ")"<< "_{" << i << "," << it->first << "," << k+1 << "}";
					// create the SCIP_VAR object
					SCIP_CALL_EXC( SCIPcreateVar(_scip, & var, namebuf.str().c_str(), 0.0, 1.0, 1.0, SCIP_VARTYPE_BINARY, TRUE, FALSE, NULL, NULL, NULL, NULL, NULL) );

					// add the SCIP_VAR object to the scip problem
					SCIP_CALL_EXC( SCIPaddVar(_scip, var) );

					// storing the SCIP_VAR pointer for later access
					_x[t][i][it->first].push_back(var);
				}
			}
		}
	}
	cerr<<"Variables are created"<<endl;

	// create constraints
	vector<int> vertex(st->graph.V());
	for(int i = 0;i<st->graph.V();++i){
		vertex[i]=i+1;
	}
	for(int t = 0; t < st->T();++t){
		vector<int> vec{};
	   	vector<bool> side(st->graph.V()+1,false);
		vector<int> term(st->getTerminals(t).begin(),st->getTerminals(t).end());
		vector<int> diff(vertex.size()-term.size());
		set_difference(vertex.begin(),vertex.end(),term.begin(),term.end(),diff.begin());
		enumerate(t,side,vec,diff,term,0);
	}

	for(int i = 1;i <= st->graph.V();i++){
		for(pair<const int, vector<FlowEdge> > & p: st->graph.adj(i)){
			for(unsigned int k = 0;k<p.second.size();++k){
				if(i>=p.first){
					continue;	
				}
				SCIP_CONS *cons;
				namebuf.str("");
				namebuf<<"cap_"<<i<<","<<p.first<<","<<k+1;
				// create SCIP_CONS object
				// capacity constraint
				SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, 0.0, (double)p.second[k].capacity(), TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE));
				for(int t = 0;t<st->T();++t){
					SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[t][i][p.first][k], (double)(st->getBandWidth(t))));
				}
				// add the constraint to scip
				SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
				// store the constraint for later on
				_cons.push_back(cons);
			}

		}
	}
}

void steiner::SteinerTreeSolver::printVector(const vector<int>& vec){
	int n = vec.size();
	for(int i = 0; i<n;++i){
		cout<<vec[i]<<" ";	
	}
	cout<<endl;
}

void steiner::SteinerTreeSolver::enumerate(int tree, vector<bool>& side, vector<int>& cur, vector<int>& v, vector<int>& t, int pos){
	int sz = cur.size();
	ostringstream namebuf;
	for(int i = pos; i < (int)t.size(); ++i){
		if(sz < (int)t.size()/2){
			cur.push_back(t[i]);
			side[t[i]] = true;
			for(long long mask = 0;mask<1LL<<(v.size());++mask){
				long long m = mask;
				vector<bool> sidetemp(side);
				vector<int> curtemp(cur);
				for(int j = 0;j<(int)v.size();++j){
					if(m&1LL){
						sidetemp[v[j]] = true;
						curtemp.push_back(v[j]);	
					}	
					m>>=1;
				}
				SCIP_CONS *cons;
				ostringstream oss;
			  	if (!cur.empty()){
				  	copy(cur.begin(), cur.end()-1, ostream_iterator<int>(oss, ","));
		 			oss << cur.back();
 				}
				namebuf.str("");
				namebuf<<"cut_"<<tree<<"_"<<oss.str()<<"_"<<mask;
				// cout<<namebuf.str()<<endl;
				// create SCIP_CONS object
				// cut constraint
				SCIP_CALL_EXC( SCIPcreateConsLinear(_scip, & cons, namebuf.str().c_str(), 0, NULL, NULL, 1.0, SCIPinfinity(_scip),TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE) );
				for(int ver = 0;ver < (int)curtemp.size();ver++){
					for(pair<const int,vector<FlowEdge> > & p: st->graph.adj(curtemp[ver])){
						if(sidetemp[p.first]){
							continue;
						}
						int smaller = min(curtemp[ver],p.first);
						int larger = max(curtemp[ver],p.first);
						for(unsigned int k = 0; k < p.second.size();++k){
							SCIP_CALL_EXC( SCIPaddCoefLinear(_scip, cons, _x[tree][smaller][larger][k], 1.0) );
							// cout<<_x[tree][smaller][larger][k]->name<<" ";
						}
					}
				}
				// add the constraint to scip
				SCIP_CALL_EXC( SCIPaddCons(_scip, cons) );
				// store the constraint for later on
				_cons.push_back(cons);
				// for(int b = 0;b<sidetemp.size();++b){
				// 	cout<<sidetemp[b]<<" ";
				// }
				// cout<<endl;
			}
			enumerate(tree,side,cur,v,t, i+1);//recursion
			cur.pop_back();
			side[t[i]] = false;
		}
	}
}


/* display the solution */
void steiner::SteinerTreeSolver::disp(std::ostream& out){
	// get the best found solution from scip
	SCIP_SOL* sol = SCIPgetBestSol(_scip);
	out << "solution for Steiner tree problem:" << endl << endl;
	if (sol == NULL){
		out << "No Solution found." << endl;
		return;
	}
	for (int t = 0; t < st->T(); ++t){
		for(map<int,map<int,vector<SCIP_VAR*> > >::iterator it1 = _x[t].begin(); it1!=_x[t].end(); ++it1){
			for(map<int,vector<SCIP_VAR* > >::iterator it2 = _x[t][it1->first].begin();it2!=_x[t][it1->first].end();++it2){
				for(unsigned int k = 0;k < _x[t][it1->first][it2->first].size();++k){
					out<<_x[t][it1->first][it2->first][k]->name<<"="<<SCIPgetSolVal(_scip, sol, _x[t][it1->first][it2->first][k])<<endl;
				}
			}
		}
	}
	out << endl;
}


/* destructor */
steiner::SteinerTreeSolver::~SteinerTreeSolver(void){
	// since the SCIPcreateVar captures all variables, we have to release them now
	for (int t = 0; t < st->T(); ++t){
		for(map<int,map<int,vector<SCIP_VAR*> > >::iterator it1 = _x[t].begin(); it1!=_x[t].end(); ++it1){
			for(map<int,vector<SCIP_VAR* > >::iterator it2 = _x[t][it1->first].begin();it2!=_x[t][it1->first].end();++it2){
				for(unsigned int k = 0;k < _x[t][it1->first][it2->first].size();++k){
					SCIP_CALL_EXC( SCIPreleaseVar(_scip, &_x[t][it1->first][it2->first][k]) );
				}
			}
		}
	}
	_x.clear();
	// the same for the constraints
	for (vector<SCIP_CONS *>::size_type i = 0; i < _cons.size(); ++i){
		SCIP_CALL_EXC( SCIPreleaseCons(_scip, &_cons[i]) );
	}
	_cons.clear();
	// after releasing all vars and cons we can free the scip problem
	// remember this has allways to be the last call to scip
	SCIP_CALL_EXC( SCIPfree( & _scip) );
}

/* solve the steiner tree problem */
void SteinerTreeSolver::solve(void){
	// this tells scip to start the solution process
	SCIP_CALL_EXC( SCIPsolve(_scip) );
}
