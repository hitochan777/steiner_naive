#!/usr/bin/env zsh

EXEC=bin/steiner
RESULT_DIR=result
MEMORY_LIMIT=4000000
TIME_LIMIT=3m
mkdir -p ${RESULT_DIR}

#for f in data/*.net
#do
#	fname=${f##*/}
#	echo "solving $f"
#	${EXEC} $f > ${RESULT_DIR}/output.${fname%.*}
#done

tp=$1
for f in ../steiner/data/${tp}*.net
do
	fname=${f##*/}
	echo "solving $f"
	ulimit -v ${MEMORY_LIMIT}
	ulimit -m ${MEMORY_LIMIT}
	nice -19 timeout ${TIME_LIMIT} ${EXEC} $f > ${RESULT_DIR}/output.${fname%.*}
done
